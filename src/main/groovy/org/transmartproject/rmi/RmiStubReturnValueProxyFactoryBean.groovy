package org.transmartproject.rmi

import org.aopalliance.intercept.MethodInterceptor
import org.aopalliance.intercept.MethodInvocation
import org.springframework.aop.framework.ProxyFactory
import org.springframework.remoting.rmi.RmiInvocationHandler
import org.springframework.remoting.rmi.RmiProxyFactoryBean
import org.springframework.remoting.support.RemoteInvocation

import java.rmi.server.RemoteObjectInvocationHandler

/**
 * Created by glopes on 3/12/14.
 */
class RmiStubReturnValueProxyFactoryBean extends RmiProxyFactoryBean {

    @Override
    protected Object doInvoke(MethodInvocation methodInvocation,
                              RmiInvocationHandler invocationHandler) {

        def superValue = super.doInvoke(methodInvocation, invocationHandler)

        if (superValue instanceof java.lang.reflect.Proxy &&
                java.lang.reflect.Proxy.getInvocationHandler(superValue)
                instanceof RemoteObjectInvocationHandler) {
            RmiInvocationHandler rih = superValue

            def proxiedInterfaces = rih.invoke(
                    new RemoteInvocation('getProxiedInterfaces',
                            [] as Class[], [] as Object[]))

            def clientProxy = new ProxyFactory(proxiedInterfaces)

            clientProxy.addAdvice({ MethodInvocation invoc ->
                doInvoke(invoc, rih)
            } as MethodInterceptor)

            clientProxy.getProxy beanClassLoader
        } else {
            superValue
        }
    }
}
