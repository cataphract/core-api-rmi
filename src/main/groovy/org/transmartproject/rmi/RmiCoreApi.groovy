package org.transmartproject.rmi

import grails.spring.BeanBuilder
import org.springframework.context.ApplicationContext
import org.transmartproject.core.dataquery.highdim.HighDimensionResource
import org.transmartproject.core.ontology.ConceptsResource

class RmiCoreApi {
    ApplicationContext appContext

    BeanBuilder createConfig() {
        def bb = new BeanBuilder()
        bb.beans {
            highDimensionResource(RmiStubReturnValueProxyFactoryBean) {
                serviceUrl = 'rmi://localhost:1199/HighDimensionResource'
                serviceInterface = HighDimensionResource
            }
        }
    }

    void init() {
        appContext = createConfig().createApplicationContext()
    }
}
