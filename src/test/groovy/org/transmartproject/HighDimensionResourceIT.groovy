import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.transmartproject.core.dataquery.DataRow
import org.transmartproject.core.dataquery.TabularResult
import org.transmartproject.core.dataquery.assay.Assay
import org.transmartproject.core.dataquery.highdim.HighDimensionResource
import org.transmartproject.core.dataquery.highdim.assayconstraints.AssayConstraint
import org.transmartproject.core.dataquery.highdim.dataconstraints.DataConstraint
import org.transmartproject.core.dataquery.highdim.projections.Projection
import org.transmartproject.rmi.RmiCoreApi

/**
 * Created by glopes on 3/12/14.
 */

class HighDimensionResourceIT {

    HighDimensionResource highDimensionResource

    @Before
    void before() {
        def rmiCoreApi = new RmiCoreApi()
        rmiCoreApi.init()
        highDimensionResource = rmiCoreApi.appContext.highDimensionResource
    }

    @Test
    void mrnaTest() {
        def mrnaResource = highDimensionResource.getSubResourceForType 'mrna'

        def projection = mrnaResource.createProjection(Projection.DEFAULT_REAL_PROJECTION)
        def trialConstraint = mrnaResource.createAssayConstraint(
                AssayConstraint.TRIAL_NAME_CONSTRAINT,
                name: 'GSE8581')
        def dataConstraint = mrnaResource.createDataConstraint(
                DataConstraint.GENES_CONSTRAINT,
                names: ['TP53'])

        TabularResult result = mrnaResource.retrieveData([trialConstraint], [dataConstraint], projection)

        println "columns label: ${result.columnsDimensionLabel}"
        println "rows label: ${result.rowsDimensionLabel}"
        println "indices label: ${result.indicesList}"

        result.getRows().each { DataRow row ->
            println "Row: ${row.label}"
            println result.indicesList.collectEntries { Assay assay ->
                [assay.patientInTrialId, row.getAt(assay)]
            }
        }
    }

}
